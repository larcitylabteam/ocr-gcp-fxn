module.exports = {
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
    ecmaFeatures: {
      modules: true,
      //experimentalObjectRestSpread: true
    }
  },
  env: {
    node: true
  },
  extends: [
    'eslint:recommended',
  ],
  rules: {
    'no-console': 0,
    //'no-extra-semi': ['error', 'always'],
    semi: ['warn', 'never']
  }
}