# About the Receipt Google Cloud Function Project

This project is part of a larger group, the **Receipt OCR Application** group in our GitLab repository. This demonstrates end to end, using a native application to capture receipt data, and process out the receipt information using a Google OCR API.

The project was guided by [https://github.com/firebase/functions-samples/blob/master/exif-images/functions/index.js](https://github.com/firebase/functions-samples/blob/master/exif-images/functions/index.js).

## File Structure

The contents of the `private` directory are NOT included in the public repository.

This script sets the config parameters for your cloud function. These parameters can be obtained from your Firebase Project (more notes to come on how to set this up, or you can google ***Creating a Firebase Web Project***). 

```bash
.
├── README.md
├── bin
│   └── deploy.sh
├── firebase.json
├── functions
│   ├── index.js
│   ├── node_modules
│   ├── package.json
│   └── yarn.lock
├── package.json
├── private
│   └── config-setup.sh
└── yarn.lock
```

### Configuring your Cloud Function

You can check out a sample of the configuration script at this gist: [https://gist.github.com/uchilaka/8ff612171536788175d7b2b27addf83c](https://gist.github.com/uchilaka/8ff612171536788175d7b2b27addf83c).

## Using the GCP Vision API

For notes on sending requests to Google's Vision API, check out the following resources:

- **Detecting document text in a local image** [https://cloud.google.com/vision/docs/detecting-fulltext](https://cloud.google.com/vision/docs/detecting-fulltext)